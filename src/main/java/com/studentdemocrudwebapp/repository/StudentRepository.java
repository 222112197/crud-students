/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.studentdemocrudwebapp.repository;

import com.studentdemocrudwebapp.entity.Student;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author hp
 */
public interface StudentRepository extends JpaRepository <Student,Long> {
    //Contoh method abstract baru
    Optional<Student> findByLastName(String lastName);
}