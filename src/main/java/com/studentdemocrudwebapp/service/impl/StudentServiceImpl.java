/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.studentdemocrudwebapp.service.impl;

import com.studentdemocrudwebapp.dto.StudentDto;
import com.studentdemocrudwebapp.entity.Student;
import com.studentdemocrudwebapp.mapper.StudentMapper;
import com.studentdemocrudwebapp.repository.StudentRepository;
import com.studentdemocrudwebapp.service.StudentService;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.stereotype.Service;

/**
 *
 * @author hp
 */
@Service
public class StudentServiceImpl implements StudentService {
    private StudentRepository studentRepository;
    
    
    public StudentServiceImpl(StudentRepository studentRepository){
        this.studentRepository = studentRepository;
    }
    
    @Override
    public List<StudentDto> ambilDaftarStudent() {
        List<Student> students = this.studentRepository.findAll();
        // Konversi obj student ke studentDto satu per satu dengan fungsi map
        List<StudentDto> studentDtos = students.stream()
                .map((student)->(StudentMapper.mapToStudentDto(student)))
                .collect(Collectors.toList());
        return studentDtos;
    }

    @Override
    public void perbaruiDataStudent(StudentDto studentDto) {
        Student student = StudentMapper.mapToStudent(studentDto);
        this.studentRepository.save(student);
    }

    @Override
    public void hapusDataStudent(Long studentId) {
        this.studentRepository.deleteById(studentId);
    }

    @Override
    public void simpanDataStudent(StudentDto studentDto) {
        Student student = StudentMapper.mapToStudent(studentDto);
        //system.out.println(student);
        studentRepository.save(student);
    }
    
    @Override
    public StudentDto cariById(Long id){
        Student student = studentRepository.findById(id).orElse(null);
        StudentDto studentDto = StudentMapper.mapToStudentDto(student);
        return studentDto;
    }
}
