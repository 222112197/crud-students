package com.studentdemocrudwebapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
//import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
//@SpringBootApplication(scanBasePackages = {"repository","repository","controller", "dto", "entity", "mapper", "service"})
//@ComponentScan({"com.studentdemocrudwebapp.controller.StudentController"})
public class StudentDemoCrudWebappApplication {

	public static void main(String[] args) {
		SpringApplication.run(StudentDemoCrudWebappApplication.class, args);
	}
}
